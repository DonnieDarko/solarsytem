﻿using UnityEngine;
using System.Collections;

public class SelfRotate : MonoBehaviour {

	/********************************************
	* Cette classe gère la rotation des objets  *
	* sur leur axe                              *
	*********************************************/

	[Range(0f,20f)]
		public float m_scale=5;
	public bool m_invert = false;

	private Vector3 Axe = Vector3.up;
	private float m_time=0f;

	// Use this for initialization

	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (m_invert) {
			m_time = -100 * Time.deltaTime * m_scale * Mathf.PI / 180;
		} else {
			m_time = 100 * Time.deltaTime * m_scale * Mathf.PI / 180;
		}
		transform.Rotate (Axe* m_time, Space.Self);
	}

	// La collision avec une planète détruit le vaisseau
	void OnTriggerEnter(Collider other) {
		Destroy(other.gameObject);
	}

	public void ChangeRotation(float newSpeed){
		m_scale = newSpeed;
	}
}
