﻿using UnityEngine;
using System.Collections;

public class ShipTrajectory : MonoBehaviour {
	/*************************************************************
	* Cette classe n'est plus utilisée                           *
	**************************************************************/
	[Range(0f,50f)] public float m_velocity=1f;
	public float m_Trajectory_lenght = 20.0f;

	private float angle = 0.0f;


	// Use this for initialization
	void Start () {
		transform.parent.localPosition = new Vector3 (-m_Trajectory_lenght/2, 0, 0);


	}
	
	// Update is called once per frame
	void Update () {

		if (transform.parent.localPosition.x >= -m_Trajectory_lenght / 2 && transform.parent.localPosition.x <= m_Trajectory_lenght / 2) {
			angle = 0f;
			transform.parent.Translate (Vector3.forward * Time.deltaTime * m_velocity, Space.Self);	
		} else if (angle <= 2*Mathf.PI) {
			angle +=0.1f*m_velocity;
			transform.parent.localPosition = new Vector3 (m_Trajectory_lenght/2, 0, 0);		
			transform.parent.RotateAround(transform.parent.localPosition, Vector3.up, angle);
		}

	}

}
