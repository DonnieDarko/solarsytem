﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
	/************************************************
	* Cette classe gère le déplacement de la caméra *
	* dans le menu d'accueil (Système solaire)      *
	************************************************/
	// L'origine est un GameObject autour duquel tourne la caméra
	public Transform m_Origine = null;
	[Range(0f,2f)]
		public float m_sensibility=1f;
	
	private float xAxisValue = 0f;
	private float zAxisValue = 0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// Gestion des mouvement de la caméra avec la souris
		xAxisValue = Input.GetAxis("Mouse X")*m_sensibility/10;
		zAxisValue = Input.GetAxis("Mouse Y")*m_sensibility/10;
		if(Camera.current != null)
		{
			transform.Translate(new Vector3(xAxisValue, 0.0f, zAxisValue));
			transform.LookAt(m_Origine);
		}
	}
}
