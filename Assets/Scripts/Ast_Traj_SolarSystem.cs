﻿using UnityEngine;
using System.Collections;
using System;

public class Ast_Traj_SolarSystem : MonoBehaviour {
	/***************************************************************************************
	* Cette classe gère la trajectoire des astéroides mais UNIQUEMENT dans l'écran du menu *
	* c'est-a-dire dans la présentation du système solaire                                 *
	****************************************************************************************/

	// curseur pour gérer la vitesse des astéroides dans l'espace
	[Range(0f,20f)] public float m_velocity = 1.0f;
	private Vector3 m_end = Vector3.zero;
	private Vector3 firstForw = Vector3.zero;

	
	// Use this for initialization
	void Start () {
		// On récupère un point aléatoire qui correspond à la direction dans laquelle partira l'objet
		UnityEngine.Random.seed = (int)DateTime.Now.Ticks;
		m_end = UnityEngine.Random.onUnitSphere * 5;
		/* Ce vecteur enregistre la 1ere direction de l'astéroide car celui-ci tourne sur lui-meme
		 * en raison d'une animation*/
		firstForw = m_end - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += firstForw * Time.deltaTime/5*m_velocity;
	}
	
}
