﻿using UnityEngine;
using System.Collections;

public class Display_astTraj : MonoBehaviour {


	public int segments; // Nombre de segments pour les trajectoires des astéroides en jeu
	public GameObject ShipPosition = null;
	public GameObject Ship = null;
	public GameObject asteroid = null;

	private LineRenderer line = null;


	void Start() {
		line = gameObject.AddComponent<LineRenderer>();
		line.enabled = Controls_InGame.m_Trajectories;
		line.SetVertexCount (2);
		line.material = new Material(Shader.Find("Particles/Additive"));
		line.SetColors (Color.green, Color.green);
		line.SetWidth (0.02f, 0.02f);
		line.useWorldSpace = false;
	}


	void Update(){
		if (line) {
			line.enabled = Controls_InGame.m_Trajectories;
			line.SetPosition (0,transform.parent.position);
			line.SetPosition(1,transform.forward*10);
		}
		// Tentative pour changer la couleur de la trajectoire lorsque le vaisseau la coupe
		// Ne fonctionne pas
		if (!Controls_InGame.gameOver) {
			if (line.bounds.Intersects(Ship.collider.bounds)) {
				line.SetColors (Color.red, Color.red);
			} else {
				line.SetColors (Color.green, Color.green);
			}
		}

	}

}
