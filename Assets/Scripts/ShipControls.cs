﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{
	public float max_horizontal=1.0f;
	public float max_vertical=1.0f;
	public float offset = 0.05f;
	//public float xMin, xMax, zMin, zMax;
}

public class ShipControls : MonoBehaviour {
	/*********************************************
	* Cette classe gère le déplacement et la     *
	* destruction du vaisseau                    *
	*********************************************/


	[Range(1f,100f)] public float m_vitesse=5.0f;
	public Transform m_Center = null;
	public Boundary boundary;

	//private Vector3 m_Position = Vector3.zero;
	private Vector3 m_direction = Vector3.zero;
	private GameObject explosion = null;
	private GameObject	game_manager = null;
	private Controls_InGame controls = null;


	// Use this for initialization
	void Start () {
		explosion = GameObject.Find ("Explosion06")as GameObject;
		game_manager = GameObject.Find ("Game Manager SolarSytem");
		controls = game_manager.GetComponent<Controls_InGame>() as Controls_InGame;
	}

	// Update is called once per frame
	void Update () {
		
		m_direction = Vector3.zero;
		/******************************************
		 * Déplacement à la souris : Abandonné 
		float x_Axis = Input.GetAxis("Mouse X");
		float y_Axis = Input.GetAxis("Mouse Y");
		m_direction.x = x_Axis;
		m_direction.z = y_Axis;
		******************************************/

		// Déplacement aux touches du clavier
		if (Input.GetKey (KeyCode.Z))
				m_direction.z = 1;
		if (Input.GetKey (KeyCode.S))
				m_direction.z = -1;
		if (Input.GetKey (KeyCode.Q)){
				m_direction.x = -1;
		}
		if (Input.GetKey (KeyCode.D)) {
				m_direction.x = 1;
		}

		transform.Translate (m_direction * Time.deltaTime * m_vitesse/10, Space.Self);

	}

	void LateUpdate(){
		float x = transform.localPosition.x;
		float y = transform.localPosition.y;
		float z = transform.localPosition.z;
		// Effet "Snake" pour faire apparaitre le vaisseau de l'autre coté en cas de 
		// dépassement des limites
		if (Mathf.Abs (transform.localPosition.x) > boundary.max_horizontal) {
			transform.localPosition = new Vector3(-x,y,z);
		}
		if (Mathf.Abs (transform.localPosition.y) > boundary.max_vertical) {
			transform.localPosition = new Vector3(x,-y,z);
		}
	}

	void OnDestroy(){
		// Destruction du vaisseau
		explosion.transform.position = transform.position;
		explosion.particleSystem.Play ();
		controls.GameOver ();
	}

}
