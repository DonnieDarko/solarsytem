﻿using UnityEngine;
using System.Collections;
using System;

public class Pop_SolarSystem : MonoBehaviour {
	/************************************************
	* Cette classe fait apparaitre les asteroides   *
	* pendant le menu du jeu                        *
	************************************************/
	public GameObject prefab=null;

	private GameObject explosion=null;
	private bool hasClone = false;
	private GameObject clone = null;
	private float lifeSpan=0f;
	private float lifeTime=0f;
	private Vector3 m_origine = Vector3.zero;
	
	// Use this for initialization
	void Start () {
		UnityEngine.Random.seed = (int)DateTime.Now.Ticks;
		explosion = GameObject.Find ("Explosion07");
	}
	
	// Update is called once per frame
	void Update () {
		
		if (!hasClone) {
			m_origine = UnityEngine.Random.onUnitSphere * 5;
			clone = (GameObject)Instantiate (prefab, m_origine, Quaternion.identity);
			hasClone = true;
			lifeTime = 0f;
			lifeSpan = (1+UnityEngine.Random.value) * 3;
		} else {
			lifeTime += Time.deltaTime;
			
			if (lifeTime >lifeSpan)
			{
				explosion.transform.position = clone.transform.position;
				explosion.particleSystem.Play();
				Destroy(clone);
				hasClone = false;
			}
			
		}
	}
	
}
