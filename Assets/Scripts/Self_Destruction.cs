﻿using UnityEngine;
using System.Collections;

public class Self_Destruction : MonoBehaviour {

	/********************************************************
	* Cette classe gère la destruction des astéroides       *
	* et la collision avec le vaisseau                      *
	*********************************************************/

	public GameObject ShipTrajCenter = null;
	public float lifeSpan=0f;
	//public ParticleSystem explosion=null;
	public bool destroyed = false;
	public bool splitted = false;
	public int segments = 20;
	public GameObject explosion=null;

	private float lifeTime=0f;
	private Collider ast_collider = null;
	private MeshRenderer ast_meshRenderer = null;
	private bool hasExploded = false;
	private LineRenderer line;


	// Use this for initialization
	void Start () {
		// Durée de vie des astéroides et récupération de l'explosion associée
		lifeSpan = (1+UnityEngine.Random.value) * 10;

		ast_collider = GetComponent<Collider> () as Collider;
		ast_meshRenderer = gameObject.GetComponentInChildren<MeshRenderer> () as MeshRenderer;

	}
	
	// Update is called once per frame
	void Update () {

		lifeTime += Time.deltaTime;

		/* Les astéroides sont détruits s'ils atteignent leur durée de vie
		 * et s'ils n'ont pas été détruits auparavant*/ 
		if (lifeTime >lifeSpan || !ast_collider.enabled)
		{
			ast_meshRenderer.enabled=false;
			if (!hasExploded){
				//GameObject Kaboom = Instantiate(explosion,transform.position,Quaternion.identity) as GameObject;
				//Destroy (Kaboom,1f);
				if (lifeTime < lifeSpan){
					//Kaboom.audio.Play ();
                    //AudioHelper.CreatePlayAudioObject(BaseManager.instance.m_sound["meteor_destruction2"].m_audioclip);
                    AudioPlayer.CreatePlayAudioObject(BaseManager.instance.m_sound["meteor_destruction1"],transform);
				}
			}
			Destroy(gameObject,0.5f);
			hasExploded=true;
		}
	}

	// Cette fonction permet de gérer les collisions par collider
	void OnTriggerEnter(Collider other) {
		Buckler.m_has_collided = true;
		if (Buckler.m_bucklers_left < 0 ){
			Destroy(other.gameObject);
            // Joue le son de destruction du vaisseau
            AudioPlayer.CreatePlayAudioObject(BaseManager.instance.m_sound["ship_destruction"], transform);
		}

	}
	



}
