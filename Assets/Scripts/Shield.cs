﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {
	/******************************************************
	* Cette classe gère le comportement du bouclier       *
	* global qui entoure le vaisseau pour un temps limité *
	******************************************************/
	static public int shields_left = 0;
	public int shields_number =3;
	public float time_Shield =0f;
	public float max_time = 5f;
	private bool shield_active = false;
	private GameObject ShieldText = null;

	// Use this for initialization
	void Start () {
		ShieldText = GameObject.Find ("ShieldText");
		shields_left = shields_number;
		ShieldInfo ();
	}
	
	// Update is called once per frame
	void Update () {
		ShieldInfo ();
		time_Shield = max_time- Time.deltaTime;
		if (Controls_InGame.m_Shield == true && !shield_active && shields_left > 0) {
			shields_left--;
            //AudioHelper.CreatePlayAudioObject(BaseManager.instance.m_sound["force_field1"].m_audioclip);
            AudioPlayer.CreatePlayAudioObject(BaseManager.instance.m_sound["force_field1"], transform);
            StartCoroutine (activate_shield ());
		}
	}

	IEnumerator activate_shield(){

		shield_active = true;
		gameObject.collider.enabled = true;
		gameObject.renderer.enabled = true;
		if (!gameObject.particleSystem.isPlaying){
			gameObject.particleSystem.Play();
		}

		yield return new WaitForSeconds (max_time);

		Controls_InGame.m_Shield = false;
		time_Shield = max_time;
		gameObject.particleSystem.Stop();
		gameObject.collider.enabled = false;
		gameObject.renderer.enabled = false;
		shield_active = false;
		Controls_InGame.m_DisabledBucklers = false;
	}

	void ShieldInfo(){
		if (shields_number < 2) {
			ShieldText.guiText.text = shields_left + " SHIELD LEFT (" + time_Shield.ToString ("F1") + " sec.)";
		} else {
			ShieldText.guiText.text = shields_left + " SHIELDS LEFT (" + time_Shield.ToString ("F1") + " sec.)";
		}
	}
}
