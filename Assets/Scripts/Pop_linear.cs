﻿using UnityEngine;
using System.Collections;
using System;

public class Pop_linear : MonoBehaviour {

	/************************************************
	* Cette classe fait apparaitre les asteroides   *
	* pendant la phase de jeu                       *
	************************************************/
	public GameObject prefab=null;		// Prefab à instancier

	private bool Ast_wave=false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (transform.parent.position);
		// appel des vagues d'asteroides
		if (Ast_wave == false) {
			StartCoroutine (Ast_pop (Difficulty.m_time_between_pop / 20));
			Ast_wave = true;
		}
	}

	IEnumerator Ast_pop(float pop_delay){
		// Apparition des vagues d'asteroides
		UnityEngine.Random.seed = (int)DateTime.Now.Ticks;

		Vector3 randPos = UnityEngine.Random.insideUnitSphere * 4 + transform.position;
		GameObject clone = Instantiate (prefab, randPos, Quaternion.identity) as GameObject;
		clone.transform.position = randPos;

		float randForce = Difficulty.m_Asteroid_velocity * (4 + UnityEngine.Random.value);
		clone.rigidbody.AddForce (transform.forward*randForce);
		yield return new WaitForSeconds(pop_delay);
		Ast_wave = false;
	}
	
}
