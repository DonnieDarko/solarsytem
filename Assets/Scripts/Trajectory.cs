﻿using UnityEngine;
using System.Collections;
using System;

public class Trajectory : MonoBehaviour {
	/**************************************************************
	* Cette classe gère la trajectoire du préfab autour du centre *
	* de rotation du vaisseau                                     *
	**************************************************************/
	public GameObject Traj_center = null;
	private GameObject SolarCenter = null;

	// Use this for initialization
	void Start () {
		UnityEngine.Random.seed = (int)DateTime.Now.Ticks;
		SolarCenter = GameObject.Find ("SolarCenter2");
		Traj_center.transform.position = SolarCenter.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	}


	void fixedUpdate(){

	}

	public GameObject bouncing_effect = null;
	void OnCollisionEnter(Collision collision) {
		ContactPoint contact = collision.contacts[0];
		Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
		Vector3 pos = contact.point;
		GameObject effect = Instantiate(bouncing_effect, pos, rot) as GameObject;
		effect.transform.position = pos;
		Destroy (effect, 1);
	}


}
