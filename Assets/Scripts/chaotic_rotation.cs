﻿using UnityEngine;
using System.Collections;
using System;

public class chaotic_rotation : MonoBehaviour {
	/*************************************************************
	* Cette classe gère la rotation des astéroides sur eux-memes *
	* dans le menu d'accueil (Système solaire)                   *
	**************************************************************/

	private Vector3 m_rot_vector = Vector3.zero;
	private float angle = 0.0f;
	private float m_velocity = 0.0f;

	// Use this for initialization
	void Start () {
		// On déterminbe aléatoirement le vecteur autour duquel tourne l'objet
		UnityEngine.Random.seed = (int)DateTime.Now.Ticks;
		m_rot_vector = UnityEngine.Random.insideUnitSphere;
		// Ainsi que sa vitesse de rotation
		m_velocity = 30+40*UnityEngine.Random.value;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeScale != 0) {
			angle += m_velocity * Time.deltaTime / 180 * Mathf.PI;
			transform.RotateAround (transform.position, m_rot_vector, angle);
		}
	}

}
