﻿//This is free to use and no attribution is required
	//No warranty is implied or given
using UnityEngine;
using System.Collections;

public class Laser: MonoBehaviour {
	/************************************************
	* Cette classe les lasers du vaisseau          *
	************************************************/
	public GameObject astPart1 = null;
	public GameObject astPart2 = null;
	public bool	desactivate = false;

	// Use this for initialization
	void Start () {
	}

	void Update () {

		// Les lasers sont créé à l'aide de particules 
		// et la visée utilise des rayons+colliders des asteroides
		if (Controls_InGame.m_LaserShoot){
            //AudioHelper.CreatePlayAudioObject(BaseManager.instance.m_sound["laser_shot"].m_audioclip, 0.2f, "LaserShot", transform.position, 1.0f);
            AudioPlayer.CreatePlayAudioObject(BaseManager.instance.m_sound["laser_shot"], transform);
            
            //AudioHelper.CreatePlayAudioObject(BaseManager.instance.Sfx_Sounds.inactive[4], 0.2f, "LaserShot", transform.position, 1.0f);
            //Debug.Log(AudioHelper.getChannel(BaseManager.instance.Sfx_Sounds.inactive[4]).m_volume);
			particleEmitter.Emit();
			RaycastHit hit;
			Ray ray = new Ray(transform.position, transform.forward);
			if (Physics.Raycast (ray, out hit) && !desactivate)
				if (hit.collider != null) {
					hit.collider.enabled = false;
					if (hit.collider.transform.childCount > 1){
						SplitAsteroid (hit.point, hit.collider.transform);
					}
				}
		}
	}

	void SplitAsteroid(Vector3 hitPoint, Transform astTransform){

		Vector3 origin1 = astTransform.GetChild (2).transform.position;
		Vector3 origin2 = astTransform.GetChild (3).transform.position;
		Material texture = astTransform.GetChild (1).renderer.material;

		GameObject part1 = Instantiate (astPart1, origin1, Quaternion.identity) as GameObject;
		part1.transform.position = origin1;
		part1.renderer.material = texture;
		part1.GetComponent<Self_Destruction> ().splitted = true;

		Vector3 vec_Force1 = origin1 - hitPoint;

		GameObject part2 = Instantiate (astPart2, origin2, Quaternion.identity) as GameObject;
		part2.transform.position = origin2;
		part2.renderer.material = texture;
		part2.GetComponent<Self_Destruction> ().splitted = true;
		Vector3 vec_Force2 = origin2 - hitPoint;
		
		part1.rigidbody.AddForce (vec_Force1*200);
		part2.rigidbody.AddForce (vec_Force2*200);
	}
}