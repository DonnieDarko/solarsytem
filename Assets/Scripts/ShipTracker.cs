﻿using UnityEngine;
using System.Collections;

public class ShipTracker : MonoBehaviour {
	/*********************************************
	* Cette classe gère la position de la caméra *
	* qui suit le vaisseau en décalé             *
	*********************************************/

	public Transform m_GoToTrack = null; // Objet à suivre
	[Range(0f,10f)] public float m_sensibility = 1.0f;

	private Vector3 goCurrentPos = Vector3.zero;
	private Vector3 CurrentPos = Vector3.zero;
	private float z_dist = 0.0f;
	private float y_pos = 0.0f;
	private float x_pos = 0.0f;

	// Use this for initialization
	void Start () {
		z_dist = transform.localPosition.z;
	}

	void Update(){
		x_pos = transform.localPosition.x;
		y_pos = transform.localPosition.y;
		transform.localPosition = new Vector3 (x_pos, y_pos, z_dist);
	}

	// Update is called once per frame
	void LateUpdate () {
		if (m_GoToTrack == null) {
			return;
		}
		CurrentPos = transform.localPosition;
		goCurrentPos = m_GoToTrack.localPosition;
		// Fonction lerp pour suivre de façon "Smooth"
		transform.localPosition= Vector3.Lerp (CurrentPos,goCurrentPos, Time.deltaTime*m_sensibility);
	}
}
