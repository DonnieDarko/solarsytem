﻿using UnityEngine;
using System.Collections;

public class IntroSceneManager : MonoBehaviour {

    // Use this for initialization
    void Start() 
    {
        Sound introMusic = BaseManager.instance.getSoundbyName("intro_music");
        Sound gameMusic = BaseManager.instance.getSoundbyName("game_music");

#if UNITY_EDITOR
        // FadeOut de la musique du jeu lors d'un changement de scène
        if (gameMusic.isActive())
        {
            Register objRegister = BaseManager.instance.getObjSoundRegister("game_music");
            objRegister.m_fade_value = -gameMusic.m_fade_value;
        }
#endif
        // Fading utilise UnityEditor : 
        // Les blocs ci-dessus et ci-dessous contournent le problème de façon temporaire pour builder
#if !UNITY_EDITOR
        if (gameMusic.isActive())
        {
            gameMusic.m_mute = true;
        }
#endif

        introMusic.m_mute = false;
        if (!introMusic.isActive())
        {
            AudioPlayer.CreatePlayAudioObject(introMusic, BaseManager.instance.transform);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
