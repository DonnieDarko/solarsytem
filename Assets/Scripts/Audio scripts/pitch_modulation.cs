﻿using UnityEngine;
using System.Collections;
using System;

#if UNITY_EDITOR
public class pitch_modulation : MonoBehaviour {
    public string m_pitch_sound_to_manage = "";
    public float m_pitch_delay = 0.0f;
    private Sound m_soundToPitch = null;
    private AudioSource m_sourceToPitch = null;

	// Use this for initialization
	void Start () {
        // Initialisation des informations relatives au son à pitcher
        if (m_pitch_sound_to_manage != "")
        {
            // Recherche du SoundObject dont le pitch doit varier
            Register m_registerToPitch = BaseManager.instance.getObjSoundRegister(m_pitch_sound_to_manage);
            Sound m_soundToPitch = BaseManager.instance.getSoundbyName(m_pitch_sound_to_manage);
            m_sourceToPitch = m_registerToPitch.GetComponent<AudioSource>();
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (m_pitch_sound_to_manage != "")
        {
            StartCoroutine(change_pitch());
        }
	}

    IEnumerator change_pitch()
    {
        UnityEngine.Random.seed = (int)DateTime.Now.Ticks;
        m_sourceToPitch.pitch = 1.0f + UnityEngine.Random.Range(-m_soundToPitch.m_pitch_range, m_soundToPitch.m_pitch_range) / 100.0f;
        yield return new WaitForSeconds(m_pitch_delay);
    }
}
#endif