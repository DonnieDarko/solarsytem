﻿using UnityEngine;
using System.Collections;

public class Display_astTraj_old : MonoBehaviour {
	
	
	public int segments; // Nombre de segments pour les trajectoires des astéroides en jeu
	public GameObject ShipPosition = null;
	public GameObject Ship = null;
	public GameObject ShipTrajCenter = null;
	public GameObject asteroid = null;
	//public float epsilon = 0.05f;
	
	private float x_pos=0f;
	private float y_pos=0f;
	private float z_pos=0f;
	private LineRenderer line = null;
	private float baseAngle =0f;
	private float distanceToaxis =0.0f;
	
	void Awake(){
		ShipTrajCenter = GameObject.Find("SolarCenter2");
	}
	
	void Start() {
		x_pos = asteroid.transform.position.x - transform.position.x;
		z_pos = asteroid.transform.position.z - transform.position.z;
		// Calcul de la distance de l'astéroide à son axe de rotation
		distanceToaxis = Mathf.Sqrt (x_pos * x_pos + z_pos * z_pos);
		y_pos = asteroid.transform.position.y - transform.position.y;
		
		// Appel de la fonction d'affichage des trajectoires en line renderers
		if (!Controls_InGame.gameOver) {
			CreatePoints (distanceToaxis, y_pos);
		}
		// Angle de base pour afficher les gizmos
		baseAngle = ShipTrajCenter.transform.eulerAngles.y*Mathf.PI/180;
	}
	
	void CreatePoints (float _radius_pos, float _y_pos)
	{
		/* Cette fonction dessine les segments d'une line renderer centrée
		 * sur l'axe de rotation de l'asteroide et passant par celui-ci */
		line = gameObject.AddComponent<LineRenderer>();
		line.enabled = Controls_InGame.m_Trajectories;
		line.SetVertexCount (segments + 1);
		line.material = new Material(Shader.Find("Particles/Additive"));
		line.SetColors (Color.green, Color.green);
		line.SetWidth (0.02f, 0.02f);
		line.useWorldSpace = false;
		float x = 0f;
		float y = _y_pos;
		float z = 0f;
		
		float angle = Mathf.PI;
		
		// Boucle de dessin de n segments pour afficher un cercle
		for (int i = 0; i < (segments + 1); i++)
		{
			x = Mathf.Sin (angle) * _radius_pos;
			z = Mathf.Cos (angle) * _radius_pos;
			
			line.SetPosition (i,new Vector3(x,y,z) );
			
			angle += (2*Mathf.PI / segments);
		}
	}
	
	void Update(){
		if (line) {
			line.enabled = Controls_InGame.m_Trajectories;
		}
		// Tentative pour changer la couleur de la trajectoire lorsque le vaisseau la coupe
		// Ne fonctionne pas
		if (!Controls_InGame.gameOver) {
			if (line.bounds.Intersects(Ship.collider.bounds)) {
				line.SetColors (Color.red, Color.red);
			} else {
				line.SetColors (Color.green, Color.green);
			}
		}
		
	}
	
	void OnDrawGizmos () {
		// Dessin de la trajectoire de l'asteroide à l'aide de Gizmos
		Vector3 SpacePoint = Vector3.zero;
		Gizmos.color = Color.green;
		
		if (Controls_InGame.m_ShowAstGizmos)
		{
			float startAngle = baseAngle+ Mathf.PI/2; // L'affichage est limité à un secteur angulaire
			float endAngle = startAngle + Mathf.PI/2; // pour préserver les performances
			for (float i=startAngle; i < endAngle; i+=0.02f) {
				SpacePoint.x = ShipTrajCenter.transform.position.x+distanceToaxis*Mathf.Sin(i);
				SpacePoint.y = asteroid.transform.position.y;
				SpacePoint.z = ShipTrajCenter.transform.position.z+distanceToaxis*Mathf.Cos(i);
				
				Gizmos.DrawSphere(SpacePoint, transform.localScale.x / 50);
			}	
		}
	}
}

