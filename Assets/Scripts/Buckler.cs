﻿using UnityEngine;
using System.Collections;

public class Buckler : MonoBehaviour {
	/**************************************************
	* Cette classe gère le comportement des boucliers *
	* protecteurs en orbite autour du vaisseau        *
	**************************************************/
	static public int m_bucklers_left = 3;
	static public bool m_has_collided = false;
	static public float m_last_impact = 0.0f;
	public int m_set_bucklers_left = 3;
	public int m_ID=0;
	public float m_time_to_respawn = 20f;

	// Use this for initialization
	void Start () {
		m_bucklers_left = m_set_bucklers_left;
	}
	
	// Update is called once per frame
	void Update () {
		// Desactive le comportement physique lorsque le bouclier protecteur global est activé
		rigidbody.isKinematic = Controls_InGame.m_DisabledBucklers;

		// Au fur et à mesure de leur destruction, les boucliers orbitaux ne sont plus affichés
		if (m_bucklers_left < m_ID) {
			renderer.enabled = false;
		} else {
			renderer.enabled = true;
		}

		// En cas de collision du vaisseau (voir Self-Destruction), on joue un système de particules
		// et on enregistre le moment de l'impact
		if (m_has_collided == true) {
			m_bucklers_left--;
			particleSystem.Play ();
			m_last_impact = Time.timeSinceLevelLoad;
			m_has_collided = false;
		}

		// Les boucliers régénèrent au bout d'un certain temps après l'impact (m_time_to respawn)
		if (m_bucklers_left < m_set_bucklers_left && (Time.timeSinceLevelLoad-m_last_impact) >= m_time_to_respawn ) {
			m_bucklers_left++;
		}

	}

}
