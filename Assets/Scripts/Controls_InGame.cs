﻿using UnityEngine;
using System.Collections;


/********************************************************************************************
* Cette classe gère les fonctions du Game Manager : Difficulté, Game Over, Highscore, Menu, *
*  ainsi que les controles généraux : Affichage des gizmos, des trajectoires, pause, etc... *
********************************************************************************************/

// Sous-menu de gestion de la difficulté
[System.Serializable]
public class Difficulty
{
	[Range(0.5f,10.0f)]
		public float set_time_between_pop = 6.0f;
	static public float m_time_between_pop = 6.0f;
	[Range(1f,1000.0f)]
		public float set_Asteroid_velocity = 20.0f;
	static public float m_Asteroid_velocity = 20.0f;
	[Range(0f,20f)]
		public float set_Ship_velocity = 5.0f;
	static public float m_Ship_velocity = 5.0f;
	public int set_difficulty_level = 0;
	static public int m_difficulty_level = 0;
	static public int m_time_to_nextLevel = 5; // Temps entre chaque palier de difficulté
}


public class Controls_InGame : MonoBehaviour {

	static public bool m_GameOnPause=false;
	static public bool gameOver = false;			
	static public bool m_ShowPlanetGizmos=false;	// Affiche les gizmos dans le menu d'accueil
	static public bool m_ShowAstGizmos=false;		// idem dans le jeu pour les astéroides
	static public bool m_LaserShoot=false;			
	static public bool m_Trajectories=false;		// Affiche les lines renderer des astéroides (trajectoires)
	static public bool m_Shield=false;
	static public bool m_DisabledBucklers=false;

	// GUItext des menus
	public GUIText ScoreText;
	public GUIText RestartText;
	public GUIText GameOverText;
	public GUIText LevelText;
	public GUIText MenuText;
	public GUIText ShieldText;
	public Difficulty difficulty;

	private bool hideMenu = false;
	private int score = 0;
	private int highscore =0;
	private int counter =0;			// Utilisé pour faire clignoter le "Restart"
	private int blinkspeed = 10;
	private GameObject SolarCenter2 = null; 
	private float baseTime_between_pop = 0.0f;
	private float baseAsteroid_velocity = 0.0f;
	private float baseShip_velocity = 0.0f;

	// Use this for initialization
	void Start () {
		// Les textes affichés sont différents entre
		// le menu d'accueil et le jeu
		if (Application.loadedLevel == 0) {
			StartMenu();
		}
		if (Application.loadedLevel == 1) {
			StartInGame();
		}
	}
	
	// Update is called once per frame
	void Update () {
		// Idem
		if (Application.loadedLevel == 0) {
			UpdateMenu();
		}
		if (Application.loadedLevel == 1) {
			UpdateInGame();
		}
		
	}


	void LateUpdate(){

		// La difficulté augmente toutes les X secondes (m_time_to_nextLevel)
		if (Application.loadedLevel == 1) {
				SetDifficulty ();
				// On affiche le texte "Level Y" pendant 1 seconde
				if (score % Difficulty.m_time_to_nextLevel == 0) {
						int step = score / Difficulty.m_time_to_nextLevel;
						raiseDifficulty (step);
				} else {
						LevelText.enabled = false;
				}
				SolarCenter2.GetComponent<SelfRotate> ().m_scale = Difficulty.m_Ship_velocity;
		}
	}

	// Fonction appelée lorsque le vaisseau est détruit
	public void GameOver(){
		// Affichage des textes en cas de Game Over
		if (GameOverText) {
			GameOverText.text = "GAME OVER";
			GameOverText.enabled = true;
			RestartText.text = "Press 'R' for RESTART";
			ScoreText.text = "Best survival time : " + highscore + " sec.";
		}
		gameOver = true;
		// et sauvegarde du HighScore
		PlayerPrefs.SetInt ("Highscore", highscore);
	}

	void SetDifficulty(){
		// On passe les variables publiques aux variables statiques
		// pour appeler les valeurs dans les autres classes
		Difficulty.m_Asteroid_velocity = difficulty.set_Asteroid_velocity;
		Difficulty.m_Ship_velocity = difficulty.set_Ship_velocity;
		Difficulty.m_time_between_pop = difficulty.set_time_between_pop;
		Difficulty.m_difficulty_level = difficulty.set_difficulty_level;
	}

	void raiseDifficulty(int step){
		counter++; // Fait clignoter le texte "Restart"
		if (counter == blinkspeed) {
			counter = 0;
			LevelText.text = "LEVEL " + step;
			LevelText.enabled = !LevelText.enabled;
		}
		difficulty.set_difficulty_level = step;
		// Avec la difficulté, on diminue le temps de pop des astéroides,
		// et on augmente leur vitesse et celle du vaisseau
		difficulty.set_time_between_pop = baseTime_between_pop*(1-(step/(5f+step)));
		difficulty.set_Asteroid_velocity = baseAsteroid_velocity + +step*2;
		difficulty.set_Ship_velocity = baseShip_velocity+step*0.1f;
	}

	void StartMenu(){
		// Menu du démarrage
		hideMenu = false;
		highscore = PlayerPrefs.GetInt ("Highscore");
		ScoreText.text = "Best survival time : " + highscore + ".sec";
		MenuText.text = "Press [C] to show Control Keys\n" + "Press [H] to hide Menu";
		RestartText.text = "Press [S] to Start";
		MenuText.enabled = true;
		LevelText.enabled = false;
		m_ShowPlanetGizmos = true;
	}
	
	void StartInGame(){
		// Menu en jeu
		highscore = PlayerPrefs.GetInt ("Highscore");
		gameOver = false;
		GameOverText.text = "";
		RestartText.text = "";
		LevelText.text = "";
		MenuText.enabled = false;
		score = 0;
		SolarCenter2 = GameObject.Find ("SolarCenter2");
		// initialisation de la difficulté
		baseTime_between_pop = difficulty.set_time_between_pop;
		baseAsteroid_velocity = difficulty.set_Asteroid_velocity;
		baseShip_velocity = difficulty.set_Ship_velocity;
		m_ShowPlanetGizmos = false;
	}

	void UpdateMenu(){
		// Fonction Update pour le menu d'accueil (Système solaire)
		MenuText.enabled = !hideMenu;
		ScoreText.enabled = !hideMenu;

		counter++; // Fait clignoter le texte "Restart"
		if (counter == blinkspeed) {
			counter = 0;
			RestartText.enabled = !RestartText.enabled;
		}
		// Gestion de la PAUSE
		if (Input.GetKeyDown(KeyCode.Space))
		{
			m_GameOnPause = !m_GameOnPause;
			if (!m_GameOnPause){
				Time.timeScale=0;
				GameOverText.text = "PAUSE";
				GameOverText.enabled = true;
			}
			else
			{
				Time.timeScale=1;
				GameOverText.enabled = false;
			}
		}
		
		// Afficher les Gizmos en jeu
		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			m_ShowPlanetGizmos = !m_ShowPlanetGizmos;
		}
		// Afficher les controles
		if (Input.GetKeyDown(KeyCode.C))
		{
			DisplayControls();
		}
		// Retour au menu principal
		if (Input.GetKeyDown(KeyCode.M))
		{
			Start();
		}
		// Afficher ou cacher les textes
		if (Input.GetKeyDown(KeyCode.H))
		{
			hideMenu = !hideMenu;
		}
		// Quitter le jeu
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
		// Commencer le jeu
		if (Input.GetKeyDown(KeyCode.S))
		{
			Application.LoadLevel(1);
		}
        // Afficher les tests du SoundManager
        if (Input.GetKeyDown(KeyCode.T))
        {
            BaseManager.instance.m_playtests=true;
        }
	}

	void UpdateInGame(){

		if (gameOver) {
			counter++; // Fait clignoter le texte "Restart"
			if (counter == blinkspeed) {
				counter = 0;
				RestartText.enabled = !RestartText.enabled;
			}
			// Relance le jeu si le joueur appuie sur R
			if (Input.GetKeyDown (KeyCode.R)) {
				score = 0;
				Application.LoadLevel (Application.loadedLevel);
			}		
		} else {
			// mise à jour du score et du highscore tant qu'on est pas "Game Over"
			score = (int)Time.timeSinceLevelLoad;
			ScoreText.text = "Survival time : " + score + " sec.";
			highscore = Mathf.Max(score,highscore);
		} 
		// PAUSE
		if (Input.GetKeyDown(KeyCode.Space))
		{
			m_GameOnPause = !m_GameOnPause;
			if (!m_GameOnPause){
				Time.timeScale=0;
				GameOverText.text = "PAUSE";
				GameOverText.enabled = true;
			}
			else
			{
				Time.timeScale=1;
				GameOverText.enabled = false;
			}
		}
		
		// Quitter le jeu
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
		// Retour au menu principal
		if (Input.GetKeyDown(KeyCode.M))
		{
			Application.LoadLevel(0);
		}
		// Fonction Tir (Récupérée par l'objet laser
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			m_LaserShoot = true;
		} else {
			m_LaserShoot = false;
		}

		if (Input.GetKeyDown (KeyCode.Mouse1)) {
			if (Shield.shields_left > 0){
				m_DisabledBucklers = true;
				m_Shield = true;
			}
		}
        // Afficher les tests du SoundManager
        if (Input.GetKeyDown(KeyCode.T))
        {
            BaseManager.instance.m_playtests = true;
        }
	}

	// Liste des controles en jeu
	void DisplayControls(){
		MenuText.text = "Use [Z][Q][S][D] to move the Space Ship\n" 
						+ "[Left Mouse] to shoot lasers\n"
						+ "[Right Mouse] to activate Protection Shield\n"
						+ "[Spacebar] to hold on pause\n" 
						+ "[M] to return to main Menu\n"
                        + "[T] to activate sounds playtest\n" + "[Esc] to Quit";
	}
}
