﻿using UnityEngine;
using System.Collections;


public class Orbite : MonoBehaviour {
	/************************************************
	* Cette classe gère l'orbite elliptique         *
	* des objets autour d'un centre donné           *
	************************************************/
	
	public Transform m_Planete = null; 	// Centre de rotation
	public float m_distance=1;
	public float m_offset=1;			// Décalage autour du centre de rotation
	public float m_FacteurEllipse=1.5f;
	public float m_angleEllipse=0.0f;	// Angle d'inclinaison de l'ellipse
	public bool m_invert = false;		// Inversion du sens de rotation
	
	private Vector3 m_Position = Vector3.zero;
	private float m_time=0;
	private float m_deltaX = 0f, m_deltaZ = 0f;
	private string Script;
	private bool showGizmos;
	// Ajustement de la vitesse de déplacement autour de l'orbite
	[Range(0f,100f)]
		public float m_scale=10;
	
	// Use this for initialization
	void Start () {
		// Calcul du point de départ
		m_Position = transform.localPosition;
		float m_x = m_Position.x;
		float m_z = m_Position.z;
		m_distance = Mathf.Sqrt(m_x*m_x + m_z*m_z);
	}
	
	// Update is called once per frame
	
	void Update () {
		// Augmentation ou diminution de l'angle selon le sens de rotation
		if (m_invert == true) {
			m_time -= 2 * Time.deltaTime * m_scale * Mathf.PI / 180;
		} else {
			m_time += 2 * Time.deltaTime * m_scale * Mathf.PI / 180;
		}
		// Calcul de la nouvelle position
		m_Position = EllipsePoint (m_time, m_distance, m_FacteurEllipse, m_offset,m_angleEllipse);
		transform.localPosition = m_Position;
	}

	void OnDrawGizmos () {

		Vector3 SpacePoint = Vector3.zero;
		Gizmos.color = Color.gray;

		// Les Gizmos sont dessinés selon l'orbite elliptique qui est utilisée pour le déplacement de l'objet
		if (Controls_InGame.m_ShowPlanetGizmos)	// mais uniquement pour les planètes
		{
		for (float i = 0; i < 2*Mathf.PI; i+=0.1f)
			{
				SpacePoint.x = transform.parent.position.x + EllipsePoint (i , m_distance, m_FacteurEllipse, m_offset,m_angleEllipse).x * transform.parent.localScale.x ;
				SpacePoint.y = transform.parent.position.y + EllipsePoint (i , m_distance, m_FacteurEllipse, m_offset,m_angleEllipse).y * transform.parent.localScale.y ;
				SpacePoint.z = transform.parent.position.z + EllipsePoint (i , m_distance, m_FacteurEllipse, m_offset,m_angleEllipse).z * transform.parent.localScale.z ;
				Gizmos.DrawSphere(SpacePoint, transform.localScale.x / 10);
			}
		}
	}


	Vector3 EllipsePoint(float Rotation_angle, float rayon, float ratio, float x_offset, float Plane_angle) {
		// Cette fonction calcule les coordonnées d'une ellipse
		Vector3 coordinates = Vector3.zero;
		Plane_angle *= Mathf.PI / 180;
		m_deltaX = Mathf.Cos (Rotation_angle);
		m_deltaZ = Mathf.Sin (Rotation_angle);
		
		coordinates.x = ratio* rayon * m_deltaX + x_offset*Mathf.Cos(Plane_angle);
		coordinates.y = coordinates.x * Mathf.Tan (Plane_angle);
		coordinates.z = rayon * m_deltaZ;
		
		return coordinates;
	}



}
